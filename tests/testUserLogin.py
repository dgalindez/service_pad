import json

from tests.baseCase import BaseCase


class TestUser(BaseCase):

    def testSucessfulLogin(self):
        email = "lordsamiel@gmail.com"
        password =  "123456789"

        payload = json.dumps({
            "email": email,
            "password": password
        })

        response = self.app.post('/login', headers={"Content-Type": "application/json"}, data=payload)

        self.assertEqual(str, type(response.json['token']))
        self.assertEqual(200, response.status_code)

