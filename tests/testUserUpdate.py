import json

from tests.baseCase import BaseCase

class testUserUpdate(BaseCase):
    
    def testSucessfulUpdate(self):
        email = "lordsamiel@gmail.com"
        password =  "123456789"

        user_payload = json.dumps({
            "email": email,
            "password": password
        })

        self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        response = self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        login_token = response.json['token']
        print(login_token)


        payload = {
            "name": "UPDATE",
            "email": "pedro.perez@gmail.com",
            "password_hash": "123456789"
        }

        response = self.app.put('/auth/11',
            headers={"Content-Type": "application/json", "X-Access-Token": f"{login_token}"},
            data=json.dumps(payload))

        self.assertEqual(205, response.status_code)