import json

from tests.baseCase import BaseCase

class TestPublicationRegister(BaseCase):

    def testSucessfulRegister(self):
        email = "lordsamiel@gmail.com"
        password =  "123456789"

        user_payload = json.dumps({
            "email": email,
            "password": password
        })

        self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        response = self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        login_token = response.json['token']
        print(login_token)


        payload = json.dumps({
            "description": "Q nos sumerge en la Europa del siglo XVI, un continente fragmentado por las guerras de religion que dieron origen al mundo moderno...",
            "title": "Q"
        })

        print(payload)

        response = self.app.post('/publications',
            headers={"Content-Type": "application/json", "X-Access-Token": f"{login_token}"},
            data=payload)

        self.assertEqual(201, response.status_code)