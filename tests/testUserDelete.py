import json

from tests.baseCase import BaseCase

class testUserDelete(BaseCase):

    def testSucessfulDelete(self):
        email = "lordsamiel@gmail.com"
        password =  "123456789"

        user_payload = json.dumps({
            "email": email,
            "password": password
        })

        self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        response = self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        login_token = response.json['token']
        print(login_token)


        response = self.app.delete('/auth/10',
            headers={"Content-Type": "application/json", "X-Access-Token": f"{login_token}"})
        
        #assert response.status_code == 204
        self.assertEqual(204, response.status_code)
