import json

from tests.baseCase import BaseCase

class TestUserRegister(BaseCase):

    def testSucessfulRegister(self):

        payload = {
            "name": "Pedro Perez",
            "email": "pedro.perez@gmail.com",
            "password_hash": "123456789"
        }

        #response = self.app.post('/auth', headers={"Content-Type": "application/json"})
        response = self.app.post('/auth',
            headers={"Content-Type": "application/json"},
            data=json.dumps(payload))

        #self.assertEqual(str, type(response.json['id']))
        self.assertEqual(201, response.status_code)