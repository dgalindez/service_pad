import unittest

from src.app import app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class BaseCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        #self.db = db.get_db()


    def tearDown(self):
        pass
        # Delete Database collections after the test is complete
        #for collection in self.db.list_collection_names():
        #    self.db.drop_collection(collection)