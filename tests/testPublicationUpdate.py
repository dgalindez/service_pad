import json

from tests.baseCase import BaseCase

class testPublicationUpdate(BaseCase):
     def testSucessfulUpdate(self):
        email = "lordsamiel@gmail.com"
        password =  "123456789"

        user_payload = json.dumps({
            "email": email,
            "password": password
        })

        self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        response = self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        login_token = response.json['token']
        print(login_token)

        payload = {
            "id": 2,
            "title": "De sangre y cenizas UPDATE",
            "description": "Apasionante y con una accion trepidante, De sangre y cenizas es una fantasia adictiva e inesperada, perfecta para los lectores de Sarah J. Maas.",
            "priority": 5,
            "status": True
        }

        response = self.app.put('/publications/2',
            headers={"Content-Type": "application/json", "X-Access-Token": f"{login_token}"},
            data=json.dumps(payload))

        self.assertEqual(205, response.status_code)