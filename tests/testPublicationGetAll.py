import json

from tests.baseCase import BaseCase

class testPublicationGetAll(BaseCase):

    def testSucessfulGetAll(self):
        email = "lordsamiel@gmail.com"
        password =  "123456789"

        user_payload = json.dumps({
            "email": email,
            "password": password
        })

        self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        response = self.app.post('/login', headers={"Content-Type": "application/json"}, data=user_payload)
        login_token = response.json['token']
        print(login_token)

        response = self.app.get('/publications',
            headers={"Content-Type": "application/json", "X-Access-Token": f"{login_token}"})

        self.assertEqual(200, response.status_code)