from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class BaseModelMixin:
    def save(self):
        db.session.add(self)
        db.session.commit()

    def save_all(data):
        db.session.add_all(data)
        db.session.commit()

    def delete(cls,id):
        db.session.query(cls).filter_by(id=id).delete()
        db.session.commit()

    def update(cls,id,data):
        db.session.query(cls).filter_by(id=id).update(data)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def get_by_id(cls, id):
        return cls.query.get(id)

    @classmethod
    def simple_filter(cls, **kwargs):
        return cls.query.filter_by(**kwargs).first()