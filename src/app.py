import os
from flask import Flask
from flask_restful import Api
from src.routes import initialize_routes
from flask_migrate import Migrate
from src.models.models import db
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URL")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
migrate = Migrate(app, db)


@app.route('/')
def hello():
    return "Hello to my app!"


initialize_routes(api)
