from src.publications.publication import PublicationsApi, PublicationsApiByID
from src.auth.auth import AuthApi, LoginApi, AuthApiById


def initialize_routes(api):
    api.add_resource(PublicationsApi, '/publications')
    api.add_resource(PublicationsApiByID, '/publications/<string:id>')
    api.add_resource(AuthApi, '/auth')
    api.add_resource(AuthApiById, '/auth/<string:id>')
    api.add_resource(LoginApi, '/login')
