from flask_marshmallow import Marshmallow
from marshmallow import fields


ma = Marshmallow()


class UserSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    email = fields.String(required=True)
    password_hash = fields.String(required=True)
    name = fields.String(required=True)
    created_at = fields.DateTime(format="%Y-%m-%d %H:%M:%S", dump_only=True)
    updated_at = fields.DateTime(format="%Y-%m-%d %H:%M:%S", dump_only=True)

class PublicationsSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    title = fields.String()
    description = fields.String()
    priority = fields.Integer()
    status = fields.Boolean()
    time_published = fields.DateTime(format="%Y-%m-%d %H:%M:%S", dump_only=True)
    created_at = fields.DateTime(format="%Y-%m-%d %H:%M:%S", dump_only=True)
    updated_at = fields.DateTime(format="%Y-%m-%d %H:%M:%S", dump_only=True)