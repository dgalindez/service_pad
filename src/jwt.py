import sys
from flask import request, Response
import jwt
import json
from functools import wraps
from src.config import Config
from src.models.models import User


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'X-Access-Token' in request.headers:
            token = request.headers['X-Access-Token']

        if not token:
            message = {'message': 'a valid token is missing.'}
            return Response(
                json.dumps(message), status=401, mimetype='application/json'
            )

        try:
            data = jwt.decode(token, Config.SECRET_KEY)
            current_user = User.query.filter_by(email=data['email']).first()
        except jwt.ExpiredSignature:
            message = {'message': 'token is expired.'}
            return Response(
                json.dumps(message), status=401, mimetype='application/json'
            )
        except jwt.InvalidTokenError:
            message = {'message': 'token is invalid.'}
            return Response(
                json.dumps(message), status=401, mimetype='application/json'
            )
        except:
            message = {'message': 'token error.'}
            return Response(
                json.dumps(message), status=401, mimetype='application/json'
            )

        return f(current_user, *args, **kwargs)
    return decorator
