from flask import request, Response
from src.models.models import Publications
from flask_restful import Resource
from datetime import datetime
from src.schemas import PublicationsSchema
from flask_sqlalchemy import SQLAlchemy
from src.jwt import token_required
import json

db = SQLAlchemy()
publicationsSchema = PublicationsSchema()


class PublicationsApi(Resource):

    @token_required
    def get(current_user, self):
        data = []
        publications = Publications.get_all()
        if not publications:
            message = {'message': 'Publications not found.'}
            return Response(
                json.dumps(message), status=404, mimetype='application/json'
            )
        for publication in publications:
            data.append(publicationsSchema.dump(publication))
        return data
        #return Response(
        #    json.dumps(data), status=200, mimetype='application/json'
        #)
        #return jsonify(data)

    @token_required
    def post(current_user, self):
        data = request.get_json()
        print(data)
        publications = []
        for item in data:
            print("item: ", item)
            new_publication = Publications(
                title=item["title"],
                description=item["description"],
                time_published=datetime.today(),
                created_at=datetime.today(),
                updated_at=datetime.today(),
            )
            publications.append(new_publication)
        Publications.save_all(publications)
        #db.session.add_all(publications)
        #db.session.commit()
        # return jsonify(data)
        return Response(
            json.dumps(data), status=201, mimetype='application/json'
        )


class PublicationsApiByID(Resource):

    @token_required
    def get(current_user, self, id):
        data = []
        if not id:
            message = {'message': 'No input id provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )
        publication = Publications.get_by_id(id)
        if not publication:
            message = {'message': 'Publication not found.'}
            return Response(
                json.dumps(message), status=404, mimetype='application/json'
            )
        data.append(publicationsSchema.dump(publication))
        return data
        #return Response(
        #    json.dumps(data), status=200, mimetype='application/json'
        #)

    @token_required
    def put(current_user, self, id):
        data = request.get_json()
        if not data:
            message = {'message': 'No input data provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )

        if not id:
            message = {'message': 'No input id provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )

        data['updated_at'] = datetime.today()
        Publications.update(Publications, id, data)
        #db.session.query(Publications).filter_by(id=id).update(data)
        #db.session.commit()
        data = {'message': 'Publication has been updated.'}
        return Response(
            json.dumps(data), status=205, mimetype='application/json'
        )

    @token_required
    def delete(current_user, self, id):
        Publications.delete(Publications, id)
        #db.session.query(Publications).filter_by(id=id).delete()
        #db.session.commit()
        data = {'message': 'Publication has been deleted.'}
        return Response(
            json.dumps(data), status=204, mimetype='application/json'
        )
