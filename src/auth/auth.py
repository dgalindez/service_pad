from flask import Response, request
from flask_restful import Resource
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
from src.schemas import UserSchema
from src.jwt import token_required
from src.models.models import User
import jwt
from src.config import Config
import json
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
userSchema = UserSchema()


class AuthApi(Resource):

    def post(self):
        data = request.get_json()
        print("Data", data)
        if not data:
            print("No data")
            message = {'message': 'No input data provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )

        if not data.get('email'):
            print("No email")
            message = {'message': 'No email provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )

        if not data.get('password_hash'):
            print("No password")
            message = {'message': 'No password provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )

        hashed_password = generate_password_hash(
            data['password_hash'], method='sha256'
        )

        data_bd = User.simple_filter(email=data.get('email'))

        if data_bd:
            print("User already exists")
            message = {'message': 'User already exists.'}
            return Response(
                json.dumps(message), status=406, mimetype='application/json'
            )

        new_user = User(
            email=data['email'],
            name=data['name'],
            password_hash=hashed_password,
            created_at=datetime.datetime.today(),
            updated_at=datetime.datetime.today()
        )
        new_user.save()

        message = {'message': 'registered successfully.'}
        return Response(
                json.dumps(message), status=201, mimetype='application/json'
            )

    @token_required
    def get(current_user, self):
        print(current_user)
        data = []
        users = User.get_all()
        if not users:
            message = {'message': 'No users found.'}
            return Response(
                json.dumps(message), status=404, mimetype='application/json'
            )
        for user in users:
            data.append(userSchema.dump(user))
        return data


class AuthApiById(Resource):

    @token_required
    def put(current_user, self, id):
        data = request.get_json()
        if not data:
            message = {'message': 'No input data provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )
        if not data.get('email'):
            message = {'message': 'No input email provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )
        if not id:
            message = {'message': 'No input id provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )

        data['updated_at'] = datetime.datetime.today()
        data['password_hash'] = generate_password_hash(
            data['password_hash'], method='sha256')
        User.update(User, id,data)

        message = {'message': 'update successfully.'}
        return Response(
            json.dumps(message), status=205, mimetype='application/json'
        )

    @token_required
    def delete(current_user, self, id):
        User.delete(User, id)
        data = {'message': 'User has been deleted.'}
        return Response(
            json.dumps(data), status=204, mimetype='application/json'
        )


class LoginApi(Resource):

    def post(self):
        auth = request.get_json()
        if not auth or not auth.get('email') or not auth.get('password'):
            message = {'message': 'No input data provided.'}
            return Response(
                json.dumps(message), status=400, mimetype='application/json'
            )
        user = User.simple_filter(email=auth.get('email'))
        #user = User.query.filter_by(email=auth.get('email')).first()

        if not user:
            message = {'message': 'User not found.'}
            return Response(
                json.dumps(message), status=404, mimetype='application/json'
            )

        if check_password_hash(user.password_hash, auth.get('password')):
            token = jwt.encode(
                {
                    'email': user.email,
                    'exp': datetime.datetime.utcnow() +
                    datetime.timedelta(minutes=30)
                },
                Config.SECRET_KEY
            )

            message = {'token': token.decode('UTF-8')}
            return Response(
                json.dumps(message), status=200, mimetype='application/json'
            )

        message = {'message': 'Wrong credentials'}
        return Response(
            json.dumps(message), status=401, mimetype='application/json'
        )
