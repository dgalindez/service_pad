Instructions:

   Create virtual environment

   Activate virtual environment

   Install dependencies: pip install requirements.txt
  
   Create DB entities:
               flask db init
               flask db migrate
               flask db upgrade
  
   Run the project: py wsgi.py (if you are on windows, for linux replace py with python3)

To run the test files:

batch: python -m unittest --buffer

per individual: py -m unittest tests\TestFile.py
